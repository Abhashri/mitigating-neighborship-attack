# Mitigating Neighborship Attack in Underwater Sensor Networks


## Description
Transmission of information through Underwater Wireless Sensor Networks(UWSN) across the ocean is one of the enabling technologies for underwater communication. These advances trigger security concerns of the underlying UWSN. Due to the lack of predictability of the movement of the nodes in such a system, secure neighbour discovery for successful information exchange is a challenge. A neighborship attack is the one which hinders neighbour discovery amongst the various nodes within the network. The wormhole attack and the Sybil attack being the prominent attacks in this category, lead to various issues if not mitigated. The consequences of these attacks can quickly scale from reduced throughput to loss of confidentiality. Moreover, conventional cryptographic algorithms are not possible to implement in a UWSN due to restrictions on the open acoustic channel and severe underwater conditions.

We propose a true-neighbour algorithm for mitigating neighborship attack in UWSN. Furthermore, the performance of this algorithm is demonstrated in UnetStack with reference to end to end packet delay, with and without implementation of the algorithm.
Networking  of  underwater  bodies  isfeasible with the Unet project. UnetStack software is required to run each Unet node that helps  communication  over  the  links.  We  require  Application  Programming  Interface  (API)  for  communication.UnetStack API is available in Java, Groovy, Python, Javascript languages.

## References
For more reference to UnetStack check out: https://unetstack.net/handbook

